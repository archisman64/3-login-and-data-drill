/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

//  solution 1
function problem1() {

    const fs = require('fs');
    const path = require('path');
    
    function createFile(fileName) {
        return new Promise((resolve, reject) => {
            if(!fileName) {
                return resolve();
            }
            fs.writeFile(fileName, 'some text', (err) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(fileName);
                }
            })
        });
    }
    
    function deleteFile(deletePath) {
        return new Promise((resolve, reject) => {
            fs.unlink(deletePath, (err) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(deletePath);
                }
            })
        })
    }
    
    function deleteFilesInOrder(deletePath1, deletePath2) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if(deletePath1 && !deletePath2){
                    deleteFile(deletePath1)
                        .then((result) => {
                            resolve('deletion successful');
                        })
                        .catch((err) => {
                            reject(err);
                        })
                } else if(deletePath2 && !deletePath1) {
                    deleteFile(deletePath2)
                        .then((result) => {
                            resolve('deletion successful');
                        })
                        .catch((err) => {
                            reject(err);
                        })
                } else if(deletePath1 && deletePath2) {
                    deleteFile(deletePath1)
                        .then((result) => {
                            return deleteFile(deletePath2)
                        })
                        .then((result) => {
                            // console.log(result);
                            resolve('both files deleted');
                        })
                        .catch((err) => {
                            reject(err);
                        })
                } else {
                    resolve('no files to delete');
                }
            }, 2000);
        })
    }
    
    const file1 = path.join(__dirname, 'file1.txt');
    const file2 = path.join(__dirname, 'file2.txt');
    
    // pass file1 and file2 paths below to the two createFile functions respectively
    Promise.all([createFile(), createFile()])
        .then((result) => {
            
            const [deletePath1, deletePath2] = result;
            console.log('files created');
            // console.log(deletePath1, deletePath2)
            return deleteFilesInOrder(deletePath1, deletePath2);
        })
        .then((result) => {
            console.log(result);
        })
        .catch((err) => {
            console.log(err);
        })
}

problem1();